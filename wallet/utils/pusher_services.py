import pusher
from django.conf import settings


class PusherService(object):
    _instance = None
    pusher_client = None

    @staticmethod
    def get_instance():
        if PusherService._instance is None:
            PusherService()
        return PusherService._instance

    def __init__(self):
        if PusherService._instance is None:
            PusherService._instance = self
            self.pusher_client = pusher.Pusher(
                app_id=settings.PUSHER_APP_ID,
                key=settings.PUSHER_KEY,
                secret=settings.PUSHER_SECRET,
                cluster=settings.PUSHER_CLUSTER,
                ssl=settings.PUSHER_SSL
            )
        else:
            raise Exception("This class is a singleton!")

    def emit(self, channel, event, data):
        self.pusher_client.trigger(channel, event, data)
