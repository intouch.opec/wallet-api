from django.urls import path, include
from rest_framework import routers
from rest_framework_simplejwt.views import TokenRefreshView

from apis.views import v1, v2

app_name = 'api'

router = routers.DefaultRouter()

api_v1_urls = ([
    path('profile/', v1.users.ProfileAPIView.as_view(), name='profile'),
    path('auth/', v1.auth.APIAuthJWT.as_view(), name='token_obtain_pair'),
    path('auth/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('dashboard/', v1.dashboards.DashboardAPIView.as_view(), name='dashboards'),
    path('banks/', v1.banks.BankListAPIView.as_view(), name='bank_list'),
    path('customers/', v1.customers.CustomerListView.as_view(), name='customer_list'),
    path('customers/<int:pk>/', v1.customers.CustomerDetailView.as_view(), name='customer_detail'),
    path('games/', v1.games.GameListAPIView.as_view(), name='game_list'),
    path('games/<int:pk>/', v1.games.GameDetailAPIView.as_view(), name='game_detail'),
    path('transactions/', v1.transactions.TransactionListAPIView.as_view(), name='transaction_list'),
    path('transactions/deposit/', v1.transactions.TransactionDepositAPIView.as_view(), name='transaction_deposit'),
    path('transactions/withdraw/', v1.transactions.TransactionWithdrawAPIView.as_view(), name='transaction_withdraw'),
    path('transactions/banks/', v1.transaction_banks.TransactionBankListAPIView.as_view(), name='transaction_bank'),
    path('transactions/banks/<int:pk>/', v1.transaction_banks.TransactionBankDetailAPIView.as_view(), name='transaction_bank_detail'),
    path('transactions/credits/', v1.transaction_credits.TransactionCreditListAPIView.as_view(), name='transaction_credit'),
    path('transactions/credits/<int:pk>/', v1.transaction_credits.TransactionCreditDetailAPIView.as_view(), name='transaction_credit_detail'),
    path('transactions/<int:pk>/', v1.transactions.TransactionDetailAPIView.as_view(), name='transaction_detail'),
    path('users/', v1.users.UserListAPIView.as_view(), name='user_list'),
    path('users/<int:pk>/', v1.users.UserDetailAPIView.as_view(), name='user_detail'),
    path('users/<int:pk>/changepassword/', v1.users.ChangePasswordAPIView.as_view(), name='user_change_password'),
    path('profile/', v1.users.ProfileAPIView.as_view(), name='profile'),
    path('groups/', v1.group_permissions.GroupListAPIView.as_view(), name='groups'),
    path('permissions/', v1.group_permissions.PermissionListAPIView.as_view(), name='permissions'),
    path('promotions/', v1.promotions.PromotionListAPIView.as_view(), name='promotion_list'),
    path('promotions/<int:pk>/', v1.promotions.PromotionDetailAPIView.as_view(), name='promotion_detail'),
    path('promotions/<int:pk>/image/', v1.promotions.PromotionDeleteImageAPIView.as_view(), name='promotion_delete_image'),
    path('systembankaccounts/', v1.system_bank_accounts.SystemBankAccountListAPIView.as_view(), name='system_bank_account_list'),
    path('systembankaccounts/<int:pk>/', v1.system_bank_accounts.SystemBankAccountDetailAPIView.as_view(), name='system_bank_account_detail'),
    path('systemcreditaccounts/', v1.system_credit_accounts.SystemAccountListAPIView.as_view(), name='system_credit_account_list'),
    path('systemcreditaccounts/<int:pk>/', v1.system_credit_accounts.SystemAccountDetailAPIView.as_view(), name='system_credit_account_detail'),
    path('customerbankaccounts/', v1.customer_bank_accounts.CustomerBankAccountListAPIView.as_view(), name='customer_bank_account_list'),
    path('customerbankaccounts/<int:pk>/', v1.customer_bank_accounts.CustomerBankAccountDetailAPIView.as_view(), name='customer_bank_account_detail'),
    path('customercreditaccounts/', v1.customer_credit_accounts.CustomerCreditAccountListAPIView.as_view(), name='customer_credit_account_list'),
    path('customercreditaccounts/<int:pk>/', v1.customer_credit_accounts.CustomerCreditAccountDetailAPIView.as_view(), name='customer_credit_account_detail'),
    path('customerlevels/', v1.customer_levels.CustomerLevelListAPIView.as_view(), name='customer_level_list'),
    path('customerlevels/<int:pk>/', v1.customer_levels.CustomerLevelDetailAPIView.as_view(), name='customer_level_detail'),
    path('settings/', v1.settings.SettingAPIView.as_view(), name='setting')
], 'v1')

api_v2_urls = ([
    path('auth/', v2.APIAuthJWT.as_view(), name='v2_token_obtain_pair'),
    path('auth/refresh/', TokenRefreshView.as_view(), name='v2_token_refresh'),
    path('register/', v2.RegisterAPIView.as_view(), name='register'),
    path('settings/', v2.SettingAPIView.as_view(), name='v2_setting'),
    path('games/', v2.GameListAPIView.as_view(), name='v2_game_list'),
    path('banks/', v2.BankListAPIView.as_view(), name='v2_bank_list'),
    path('promotions/', v2.PromotionListAPIView.as_view(), name='v2_promotion_list'),
    path('customercreditaccounts/', v2.CustomerCreditAccountListAPIView.as_view(), name='v2_customer_credit_accounts'),
    path('customerbankaccounts/', v2.CustomerBankAccountListAPIView.as_view(), name='v2_customer_bank_accounts'),
    path('transactions/deposit/', v2.TransactionDepositAPIView.as_view(), name='v2_transaction_deposit'),
    path('transactions/withdraw/', v2.TransactionWithdrawAPIView.as_view(), name='v2_transaction_withdraw'),
    path('systembankaccounts/', v2.system_bank_accounts.SystemBankAccountListAPIView.as_view(),
         name='system_bank_account_list'),
], 'v2')

urlpatterns = [
    path('v1/', include(api_v1_urls)),
    path('v2/', include(api_v2_urls)),
    path('', include(router.urls))
]
