from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apis.serializers import CustomerCreditAccountSerializer
from apps.models import CustomerCreditAccount
from wallet.utils import STATUS_APPROVED, STATUS_PENDING
from wallet.utils.pusher_services import PusherService


class CustomerCreditAccountListAPIView(APIView):

    def get_queryset(self, user):
        customer = user.customer
        customer_credit_accounts = customer.customercreditaccount_set.all()
        filter_type = self.request.query_params.get('filterType')
        if filter_type:
            customer_credit_accounts = customer_credit_accounts.filter(status=STATUS_APPROVED)
        return customer_credit_accounts

    def get(self, request, *args, **kwargs):
        customer_credit_accounts = self.get_queryset(request.user)
        customer_credit_account_list = []
        for customer_credit_account in customer_credit_accounts:
            customer_credit_account_list.append(customer_credit_account.context_data)
        return Response(customer_credit_account_list, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        customer_credit_account_id = request.data.get('customer_credit_account_id')
        if customer_credit_account_id:
            data = {
                'status': STATUS_PENDING
            }
            customer_credit_account = CustomerCreditAccount.objects.get(pk=customer_credit_account_id)
            customer_credit_account_serializer = CustomerCreditAccountSerializer(customer_credit_account, data=data, partial=True)
            customer_credit_account_serializer.is_valid(raise_exception=True)
            customer_credit_account = customer_credit_account_serializer.save()
            context = customer_credit_account.context_data
            self.emit_message(context=context)
            return Response(context, status=status.HTTP_201_CREATED)

        customer = request.user.customer

        request_data = {
            **request.data,
            'customer': {'id': customer.pk}
        }
        customer_credit_account_serializer = CustomerCreditAccountSerializer(data=request_data)
        customer_credit_account_serializer.is_valid(raise_exception=True)

        game_id = customer_credit_account_serializer.validated_data['game']['id']
        customer_credit_account = customer.customercreditaccount_set.filter(game_id=game_id)
        if customer_credit_account.exists():
            return Response({'massage': 'This credit account already exists.'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            customer_credit_account = customer_credit_account_serializer.save()
            context = customer_credit_account.context_data
            self.emit_message(context=context)
            return Response(context, status=status.HTTP_201_CREATED)

    def emit_message(self, context):
        pusher_service = PusherService.get_instance()
        message = {'message': 'มีคำขอเปิดบัญชีใหม่'}
        pusher_service.emit('account-request', 'new', context)
        pusher_service.emit('system', 'alert', message)
