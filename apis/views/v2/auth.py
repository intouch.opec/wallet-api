from tokenize import TokenError
from django.utils.translation import ugettext_lazy as _

from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.exceptions import InvalidToken
from rest_framework_simplejwt.views import TokenObtainPairView

from apis.serializers import CustomerSerializer
from apps.models import User
from wallet.utils.pusher_services import PusherService


class APIAuthJWT(TokenObtainPairView):
    
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
            user = serializer.user
            if hasattr(user, 'customer') is False:
                raise TokenError()
        except TokenError as e:
            raise InvalidToken(
                _('username/password mismatch or invalid token. Please make sure you have entered it correctly.'),
                'invalid_credentials'
            )

        response = {
            **serializer.validated_data,
            **user.customer.context_data
        }
        return Response(response, status=status.HTTP_200_OK)


class RegisterAPIView(APIView):
    permission_classes = [AllowAny]

    def post(self, request, *args, **kwargs):
        request_data = {
            **request.data,
            'customer_level': {'id': 1}
        }
        customer_serializer = CustomerSerializer(data=request_data)
        customer_serializer.is_valid(raise_exception=True)
        customer = customer_serializer.save()
        self.emit_message()
        return Response(customer.context_data, status=status.HTTP_201_CREATED)

    def get_customer_context_data(self, customer):
        user = customer.user
        return {
            'id': customer.pk,
            'email': user.email,
            'username': user.username,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'fullname': user.get_full_name(),
            'telephone': customer.telephone,
            'line_id': customer.line_id
        }

    def emit_message(self):
        pusher_service = PusherService.get_instance()
        message = {'message': 'มีลูกค้าสมัครสมาชิกใหม่'}
        pusher_service.emit('system', 'alert', message)
