from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.models import Game


class GameListAPIView(APIView):

    def get_queryset(self):
        customer = self.request.user.customer
        game_ids = customer.customercreditaccount_set.values_list('game_id', flat=True)
        queryset = Game.objects.exclude(pk__in=game_ids)
        return queryset

    def get(self, request, *args, **kwargs):
        games = self.get_queryset()
        game_list = []
        for game in games:
            game_list.append(game.context_data)
        return Response(game_list, status=status.HTTP_200_OK)
