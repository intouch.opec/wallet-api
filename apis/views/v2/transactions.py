from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apis.serializers import TransactionDepositSerializer, TransactionCreditSerializer
from wallet.utils.pusher_services import PusherService


class TransactionDepositAPIView(APIView):

    def post(self, request, *args, **kwargs):
        transaction_serializer = TransactionDepositSerializer(data=request.data)
        transaction_serializer.is_valid(raise_exception=True)
        transaction = transaction_serializer.save()
        transaction.customer = request.user.customer
        transaction.save()
        self.emit_message(transaction)
        return Response(status=status.HTTP_201_CREATED)

    def emit_message(self, transaction, emit_event='new'):
        pusher_service = PusherService.get_instance()
        message = {'message': 'มีรายการการเงินใหม่'}
        pusher_service.emit('transaction-bank', emit_event, transaction.transactionbank.context_data)
        pusher_service.emit('system', 'alert', message)


class TransactionWithdrawAPIView(APIView):

    def post(self, request, *args, **kwargs):
        transaction_serializer = TransactionCreditSerializer(data=request.data)
        transaction_serializer.is_valid(raise_exception=True)
        transaction = transaction_serializer.save()
        transaction.customer = request.user.customer
        transaction.save()
        self.emit_message(transaction)
        return Response(status=status.HTTP_201_CREATED)

    def emit_message(self, transaction, emit_event='new'):
        pusher_service = PusherService.get_instance()
        message = {'message': 'มีรายการเครดิตใหม่'}
        pusher_service.emit('transaction-credit', emit_event, transaction.transactioncredit.context_data)
        pusher_service.emit('system', 'alert', message)
