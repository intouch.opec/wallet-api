from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apis.serializers import CustomerBankAccountSerializer
from apps.models import CustomerBankAccount


class CustomerBankAccountListAPIView(APIView):
    def get_queryset(self):
        customer_id = self.request.query_params.get('customer_id')
        queryset = CustomerBankAccount.objects.select_related('bank')
        if customer_id:
            queryset = queryset.filter(customer_id=customer_id, is_active=True)
        return queryset

    def get(self, request, *args, **kwargs):
        customer_bank_account_list = []
        customer_bank_accounts = self.get_queryset()
        for customer_bank_account in customer_bank_accounts:
            customer_bank_account_list.append(customer_bank_account.context_data)

        return Response(customer_bank_account_list, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        customer_bank_account_serializer = CustomerBankAccountSerializer(data=request.data)
        customer_bank_account_serializer.is_valid(raise_exception=True)
        customer_bank_account = customer_bank_account_serializer.save()
        return Response(customer_bank_account.context_data, status=status.HTTP_201_CREATED)


class CustomerBankAccountDetailAPIView(APIView):
    def get_queryset(self):
        pk = self.kwargs.get('pk')
        queryset = CustomerBankAccount.objects.get(pk=pk)
        return queryset

    def put(self, request, *args, **kwargs):
        customer_bank_account = self.get_queryset()
        customer_bank_account_serializer = CustomerBankAccountSerializer(customer_bank_account, data=request.data, partial=True)
        customer_bank_account_serializer.is_valid(raise_exception=True)
        customer_bank_account = customer_bank_account_serializer.save()
        return Response(customer_bank_account.context_data, status=status.HTTP_200_OK)
