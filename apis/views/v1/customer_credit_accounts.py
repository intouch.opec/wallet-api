from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apis.serializers import CustomerCreditAccountSerializer
from apps.models import CustomerCreditAccount
from wallet import utils
from wallet.utils.pusher_services import PusherService


class CustomerCreditAccountListAPIView(APIView):
    def get_queryset(self):
        queryset = CustomerCreditAccount.objects.select_related('game')
        customer_credit_account_status = self.request.query_params.get('status', '').lower()
        if customer_credit_account_status == 'approved':
            queryset = queryset.filter(status=utils.STATUS_APPROVED)
        elif customer_credit_account_status == 'pending':
            queryset = queryset.filter(status=utils.STATUS_PENDING)
        return queryset

    def get(self, request, *args, **kwargs):
        customer_credit_account_list = []
        customer_credit_accounts = self.get_queryset()
        for customer_credit_account in customer_credit_accounts:
            customer_credit_account_list.append(customer_credit_account.context_data)
        return Response(customer_credit_account_list, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        customer_credit_account_serializer = CustomerCreditAccountSerializer(data=request.data)
        customer_credit_account_serializer.is_valid(raise_exception=True)
        customer_credit_account = customer_credit_account_serializer.save()
        customer_credit_account.status = utils.STATUS_APPROVED
        customer_credit_account.save()
        self.emit_message(customer_credit_account=customer_credit_account)
        return Response(customer_credit_account.context_data, status=status.HTTP_201_CREATED)

    def emit_message(self, customer_credit_account):
        pusher_service = PusherService.get_instance()
        pusher_service.emit('customer', 'new', customer_credit_account.context_data)


class CustomerCreditAccountDetailAPIView(APIView):
    def get_queryset(self):
        pk = self.kwargs.get('pk')
        queryset = CustomerCreditAccount.objects.get(pk=pk)
        return queryset

    def get(self, request, *args, **kwargs):
        self.object = self.get_queryset()
        context = self.get_context_data()
        return Response(context, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        customer_credit_account = self.get_queryset()
        customer_credit_account_serializer = CustomerCreditAccountSerializer(customer_credit_account, data=request.data, partial=True)
        customer_credit_account_serializer.is_valid(raise_exception=True)
        customer_credit_account = customer_credit_account_serializer.save()

        customer_credit_account_status = request.data.get('status')
        if customer_credit_account_status in [utils.STATUS_APPROVED, utils.STATUS_REJECTED]:
            self.emit_message(customer_credit_account, action_type='remove')
        if customer_credit_account_status == utils.STATUS_APPROVED:
            self.emit_message(customer_credit_account, action_type='new')

        return Response(customer_credit_account.context_data, status=status.HTTP_200_OK)

    def emit_message(self, customer_credit_account, action_type='new'):
        pusher_service = PusherService.get_instance()
        context = customer_credit_account.context_data
        if action_type == 'new':
            pusher_service.emit('customer', 'new', context)
            transactions = customer_credit_account.transaction_set.select_related('transactioncredit').filter(transactioncredit__status=utils.STATUS_PENDING).exclude(transactioncredit__isnull=True)
            for transaction in transactions:
                pusher_service.emit('transaction-credit', 'update', transaction.transactioncredit.context_data)
        elif action_type == 'remove':
            pusher_service.emit('account-request', 'remove', context)

    def get_context_data(self):
        customer_credit_account = self.object
        customer = customer_credit_account.customer

        history_list = []
        transactions = customer.transaction_set.order_by('-id')[:10]
        for transaction in transactions:
            history_list.append({
                'state': transaction.state,
                'amount': transaction.amount,
                'status': transaction.status,
                'created_at': transaction.created_at
            })

        customer_context = customer.context_data
        customer_context['transaction_history'] = history_list

        context = customer_credit_account.context_data
        context['customer'] = customer_context
        return context

