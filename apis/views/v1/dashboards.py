import datetime

from django.db.models import Sum
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView

from apps.models import Transaction, Customer, SystemBankAccount, SystemCreditAccount
from wallet import utils
from wallet.utils import STATE_DEPOSIT, STATE_WITHDRAW, STATUS_APPROVED, STATUS_PENDING


class DashboardAPIView(APIView):
    def get_today_range(self):
        today_min_time = datetime.datetime.combine(datetime.date.today(), datetime.time.min)
        today_max_time = datetime.datetime.combine(datetime.date.today(), datetime.time.max)
        return today_min_time, today_max_time

    def get_transaction_instance(self, date_range=False):
        queryset = Transaction.objects
        if date_range:
            queryset = queryset.filter(created_at__range=self.get_today_range())
        return queryset

    def get_aggregate_transaction(self, transaction_state, transaction_status):
        return self.get_transaction_instance(True).filter(state=transaction_state, status=transaction_status)

    def get(self, request, *args, **kwargs):
        deposit_amount = self.get_aggregate_transaction(STATE_DEPOSIT, STATUS_APPROVED).aggregate(Sum('amount')).get('amount__sum')
        withdrawal_amount = self.get_aggregate_transaction(STATE_WITHDRAW, STATUS_APPROVED).aggregate(Sum('amount')).get('amount__sum')
        bonus_credit = self.get_aggregate_transaction(STATE_DEPOSIT, STATUS_APPROVED).aggregate(Sum('transactioncredit__bonus_credit')).get('transactioncredit__bonus_credit__sum')

        system_bank_account_list = []
        system_bank_accounts = SystemBankAccount.objects.select_related('bank').filter(is_active=True)
        for system_bank_account in system_bank_accounts:
            transaction_waiting_balance = system_bank_account.transactionbank_set \
                .filter(transaction__customer__isnull=True, transaction__state=utils.STATE_DEPOSIT) \
                .aggregate(Sum('amount')).get('amount__sum')

            context_data = system_bank_account.context_data
            if transaction_waiting_balance is None:
                context_data['waiting_balance'] = 0
                context_data['total_balance'] = context_data['balance']
            else:
                context_data['waiting_balance'] = transaction_waiting_balance
                context_data['total_balance'] = context_data['balance'] + transaction_waiting_balance
            system_bank_account_list.append(context_data)

        system_credit_account_list = []
        system_credit_accounts = SystemCreditAccount.objects.select_related('game').filter(is_active=True)
        for system_credit_account in system_credit_accounts:
            total_bonus_credit = system_credit_account.customercreditaccount_set\
                .filter(transaction__created_at__range=self.get_today_range())\
                .aggregate(Sum('transaction__transactioncredit__bonus_credit'))
            system_credit_account_list.append({
                **system_credit_account.context_data,
                'total_bonus_credit': total_bonus_credit.get('transaction__transactioncredit__bonus_credit__sum')
            })

        transaction_history_list = []
        for transaction in self.get_transaction_instance().order_by('-created_at')[:10]:
            transaction_history_list.append(transaction.context_data)

        context = {
            'transactions': transaction_history_list,
            'customer_total': Customer.objects.count(),
            'transaction_deposit_amount': deposit_amount,
            'transaction_withdraw_amount': withdrawal_amount,
            'bonus_credit_amount': bonus_credit,
            'system_bank_accounts': system_bank_account_list,
            'system_credit_accounts':  system_credit_account_list
        }
        return Response(context, status.HTTP_200_OK)
