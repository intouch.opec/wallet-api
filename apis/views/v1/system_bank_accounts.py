from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apis.serializers import SystemBankAccountSerializer
from apis.utils import has_permission
from apps.models import SystemBankAccount, LogSystemBankAccount
from wallet import utils


class SystemBankAccountListAPIView(APIView):
    def get_queryset(self):
        queryset = SystemBankAccount.objects.select_related('bank').prefetch_related('customer_levels')
        exclude_inactive_status = self.request.query_params.get('is_active', False)
        if exclude_inactive_status:
            queryset = queryset.filter(is_active=True)
        return queryset.order_by('-is_active')

    def get(self, request, *args, **kwargs):
        system_bank_account_list = []
        system_bank_accounts = self.get_queryset()
        for system_bank_account in system_bank_accounts:
            context = self.get_system_bank_account_context(system_bank_account)
            system_bank_account_list.append(context)
        return Response(system_bank_account_list, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_system_account')

        system_bank_account_serializer = SystemBankAccountSerializer(data=request.data)
        system_bank_account_serializer.is_valid(raise_exception=True)
        system_bank_account = system_bank_account_serializer.save()

        LogSystemBankAccount.objects.create(
            old_balance=0,
            new_balance=system_bank_account.balance,
            user=request.user,
            system_bank_account=system_bank_account,
            action=utils.ACTION_INITIAL_BALANCE
        )

        context = self.get_system_bank_account_context(system_bank_account)
        return Response(context, status=status.HTTP_201_CREATED)

    def get_system_bank_account_context(self, system_bank_account):
        customer_levels = []
        for customer_level in system_bank_account.customer_levels.all():
            customer_levels.append(customer_level.context_data)

        context = system_bank_account.context_data
        context['customer_levels'] = customer_levels
        return context


class SystemBankAccountDetailAPIView(APIView):
    def get_queryset(self):
        pk = self.kwargs.get('pk')
        queryset = SystemBankAccount.objects.select_related('bank').prefetch_related('customer_levels').get(pk=pk)
        return queryset

    def get(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_system_account')

        self.object = self.get_queryset()
        context = self.get_context_data()
        return Response(context, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_system_account')

        system_bank_account = self.get_queryset()
        system_bank_account_old_balance = system_bank_account.balance

        system_bank_account_serializer = SystemBankAccountSerializer(system_bank_account, data=request.data, partial=True)
        system_bank_account_serializer.is_valid(raise_exception=True)
        self.object = system_bank_account_serializer.save()

        LogSystemBankAccount.objects.create(
            old_balance=system_bank_account_old_balance,
            new_balance=self.object.balance,
            user=request.user,
            system_bank_account=self.object,
            action=utils.ACTION_UPDATE_BALANCE
        )

        context = self.get_context_data()
        return Response(context, status=status.HTTP_200_OK)

    def delete(self, request, *args, **kwargs):
        has_permission(request, 'apps.can_access_system_account')

        system_account = self.get_queryset()
        system_account.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_context_data(self):
        system_bank_account = self.object
        customer_levels = []
        for customer_level in system_bank_account.customer_levels.all():
            customer_levels.append(customer_level.context_data)

        context = system_bank_account.context_data
        context['customer_levels'] = customer_levels
        return context
