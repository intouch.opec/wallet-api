from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apis.utils import has_permission
from apps.models import Setting


class SettingAPIView(APIView):
    filter_list = [
        'line_id',
        'line_qr_code_url'
    ]

    def check_has_permission(self):
        has_permission(self.request, 'apps.can_access_setting')

    def get_queryset(self):
        queryset = Setting.objects.filter(key__in=self.filter_list)
        return queryset

    def get(self, request, *args, **kwargs):
        self.check_has_permission()

        system_settings = self.get_queryset()
        context = self.get_context_data(system_settings)
        return Response(context, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        self.check_has_permission()

        system_settings = self.get_queryset()
        for system_setting in system_settings:
            system_setting.value = request.data.get(system_setting.key, '')
            system_setting.save()

        context = self.get_context_data(system_settings)
        return Response(context, status=status.HTTP_201_CREATED)

    def get_context_data(self, system_settings):
        context = {}
        for system_setting in system_settings:
            context[system_setting.key] = system_setting.value
        return context
