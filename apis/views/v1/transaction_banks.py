from django.utils import timezone
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from safedelete import HARD_DELETE

from apis.utils import has_permission
from apps.models import TransactionBank, TransactionCredit, SystemBankAccount, LogSystemBankAccount
from wallet import utils
from wallet.utils.pusher_services import PusherService


class TransactionBankListAPIView(APIView):
    def check_has_permission(self):
        has_permission(self.request, 'apps.can_access_transaction_bank')

    def get_queryset(self):
        queryset = TransactionBank.objects.filter(status=utils.STATUS_PENDING).select_related(
            'user', 'system_bank_account', 'system_bank_account__bank', 'transaction',
            'transaction__customer', 'transaction__customer_bank_account', 'transaction__customer_bank_account__bank'
        )
        return queryset

    def get(self, request, *args, **kwargs):
        self.check_has_permission()

        transaction_bank_list = []
        transaction_banks = self.get_queryset()
        for transaction_bank in transaction_banks:
            transaction_bank_list.append(transaction_bank.context_data)
        return Response(transaction_bank_list)


class TransactionBankDetailAPIView(APIView):
    def check_has_permission(self):
        has_permission(self.request, 'apps.can_access_transaction_bank')

    def get_queryset(self):
        pk = self.kwargs.get('pk')
        queryset = TransactionBank.objects.filter(status=utils.STATUS_PENDING).select_related(
            'user', 'system_bank_account', 'system_bank_account__bank', 'transaction',
            'transaction__customer', 'transaction__customer_bank_account', 'transaction__customer_bank_account__bank'
        ).get(pk=pk)
        return queryset

    def get(self, request, *args, **kwargs):
        self.check_has_permission()

        transaction_bank = self.get_queryset()
        history_list = []
        customer = transaction_bank.transaction.customer
        if customer:
            transactions = customer.transaction_set.order_by('-id')[:10]
            for transaction in transactions:
                history_list.append({
                    'state': transaction.state,
                    'amount': transaction.amount,
                    'status': transaction.status,
                    'created_at': transaction.created_at
                })

        context = transaction_bank.context_data
        context['transaction_history'] = history_list
        return Response(context, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        self.check_has_permission()

        remark = request.data.get('remark')
        is_approved = request.data.get('is_approved', False)
        system_bank_account_id = request.data.get('system_bank_account_id')

        transaction_bank = self.get_queryset()
        transaction = transaction_bank.transaction

        if is_approved:
            amount = transaction_bank.amount
            customer_credit_account = transaction.customer_credit_account
            system_bank_account_old_balance = 0
            if transaction.state == utils.STATE_DEPOSIT:
                transaction_bank.status = utils.STATUS_APPROVED
                transaction_bank.user = request.user
                transaction_bank.save()

                system_bank_account = transaction_bank.system_bank_account
                system_bank_account_old_balance = system_bank_account.balance
                system_bank_account.balance = str(float(system_bank_account_old_balance) + float(amount))
                system_bank_account.save()

                credit = int(amount) / customer_credit_account.game.convert
                transaction_credit = TransactionCredit(credit=credit, total_credit=credit, transaction=transaction)
                transaction_credit.save()
                self.emit_message(transaction)
            else:
                system_bank_account = SystemBankAccount.objects.get(pk=system_bank_account_id, is_active=True)
                system_bank_account_old_balance = system_bank_account.balance

                transaction_bank.status = utils.STATUS_APPROVED
                transaction_bank.transaction_at = timezone.now()
                transaction_bank.system_bank_account = system_bank_account
                transaction_bank.user = request.user
                transaction_bank.save()

                system_bank_account.balance = str(float(system_bank_account_old_balance) - float(amount))
                system_bank_account.save()

                transaction.status = utils.STATUS_APPROVED
                transaction.save()
                self.emit_message(transaction, emit_event='remove')

            LogSystemBankAccount.objects.create(
                old_balance=system_bank_account_old_balance,
                new_balance=system_bank_account.balance,
                user=request.user,
                transaction=transaction,
                system_bank_account=system_bank_account,
                action=utils.ACTION_UPDATE_BALANCE
            )
            return Response({'message': 'Transaction bank was approved.'}, status=status.HTTP_200_OK)
        else:
            transaction_bank.status = utils.STATUS_REJECTED
            transaction_bank.user = request.user
            transaction_bank.save()

            transaction.status = utils.STATUS_REJECTED
            transaction.remark = remark
            transaction.save()
            self.emit_message(transaction, emit_event='remove')
            return Response({'message': 'Transaction bank was rejected.'}, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):
        self.check_has_permission()

        action = request.data.get('action')
        if action == 'updateProgress':
            transaction_bank = self.get_queryset()
            transaction_bank.user = request.user
            transaction_bank.save()
            self.emit_message(transaction_bank.transaction, emit_event='update')
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def delete(self, request, *args, **kwargs):
        self.check_has_permission()

        transaction_bank = self.get_queryset()
        transaction = transaction_bank.transaction
        if transaction.customer is None:
            self.emit_message(transaction, emit_event='remove')
            transaction.delete(force_policy=HARD_DELETE)
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def emit_message(self, transaction, emit_event='new'):
        pusher_service = PusherService.get_instance()
        emit_channel_bank = 'transaction-bank'
        emit_channel_credit = 'transaction-credit'
        if emit_event == 'new':
            pusher_service.emit(emit_channel_credit, 'new', transaction.transactioncredit.context_data)
            pusher_service.emit('system', 'alert', {'message': 'มีรายการเครดิตใหม่'})
        elif emit_event == 'update':
            pusher_service.emit(emit_channel_bank, 'update', transaction.transactionbank.context_data)

        if emit_event in ['new', 'remove']:
            pusher_service.emit(emit_channel_bank, 'remove', {'id': transaction.transactionbank.id})
